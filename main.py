import hashlib
import json
import os
import sys
from datetime import datetime

from git import Repo

from db.database import database_factory
from pipeline.pipeline_handler_factory import pipeline_handler_factory

# 1 - GET DB CONNECTION AND CREATE DB TABLES
print('INITIALISING DB CONNECTION...')
db = database_factory(os.getenv("DATABASE"), os.getenv("DATABASE_URL"))
print('DB CONNECTING AND GENERATING DB TABLES')
db.create_tables()
print('DB SETUP COMPLETE')

# 2 - READ GLOBAL ENV VARIABLES
print('LOADING ENVIRONMENT VARIABLES...')
data_location = os.getenv("DATA_LOCATION")
ci_job_name = os.getenv("CI_JOB_NAME")
ci_pipeline_id = int(os.getenv("CI_PIPELINE_ID"))
ci_previous_job_hash = os.getenv("EXECUTED_JOB_HASH")

# 3 - READ ENV INPUTS/OUTPUTS FOR THIS JOB
handler = os.getenv("HANDLER")
inputs = os.getenv("INPUTS", "").split()
outputs = os.getenv("OUTPUTS", "").split()
version = os.getenv("VERSION")

# 4 - LOAD JOB HANDLER CLASS
handler_class = pipeline_handler_factory(handler)

# 5 - BUILD METADATA FOR THIS JOB AND GENERATE A UNIQUE HASH FROM THE DICTIONARY
repo = Repo(".")
# same as: git rev-list -1 HEAD pipeline/data_processing/
file_path = handler.partition(".")[0].replace(".", "/")
commit = next(repo.iter_commits(repo.head, "pipeline/" + file_path, max_count=1))

metadata = {
    "hex_sha": commit.hexsha,
    "data_location": data_location,
    "inputs": inputs,
    "outputs": outputs,
    "version": version,
    "ci_job_name": ci_job_name,
    "ci_previous_job_hash": ci_previous_job_hash
}

# fill metadata with job specific variables
metadata = handler_class.fill_metadata(metadata=metadata)

job_hash = hashlib.sha256(json.dumps(metadata).encode("utf8")).hexdigest()

print(f"JOB WITH hash {job_hash} REQUESTED WITH metadata: {metadata}")

# 6 - LOAD JOB CACHE
print('LOADING JOB CACHE...')
found = db.find_job(job=ci_job_name, job_hash=job_hash)
# 7 - IF JOB IS CACHED, SKIP IT
if found:
    print('JOB CACHE FOUND. SKIPPING JOB.')
    db.update_ci_pipeline_id(ci_pipeline_id, job_hash)
    with open(os.getenv("CI_PROJECT_DIR") + "/job.env", "at") as out:
        out.write(f"EXECUTED_JOB_HASH={job_hash}\n")
    os.system('ls -l ' + data_location + '/' + job_hash)
    sys.exit()

# 8 - CREATE JOB DIRECTORY
os.makedirs(data_location + "/" + job_hash, exist_ok=True)

# 9 - RUN THE JOB HANDLER
result = handler_class.handle(
    volume_path=data_location,
    job_inputs=inputs,
    job_outputs=outputs,
    job_metadata=metadata,
    job_data_location=job_hash,
    ci_pipeline_id=ci_pipeline_id,
    previous_job_data_location=ci_previous_job_hash
)
os.system('ls -l ' + data_location + '/' + job_hash)

# 10 - VALIDATE AND SAVE RESULTS
if result['success'] is False:
    raise RuntimeError("EXECUTION ERROR, JOB HANDLER FAILED")

db.save_job(job_hash=job_hash, job=ci_job_name, ci_pipeline_id=ci_pipeline_id, metadata=metadata)

with open(os.getenv("CI_PROJECT_DIR") + "/job.env", "at") as out:
    out.write(f"EXECUTED_JOB_HASH={job_hash}\n")
