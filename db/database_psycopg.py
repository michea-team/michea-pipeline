import json

import psycopg
# from psycopg.errors import SerializationFailure, Error
from psycopg.rows import dict_row

from db.database import Database


class DatabasePsycopg(Database):
    def get_connection(self):
        if self.connection is not None and self.connection.closed is False:
            return self.connection
        self.connection = psycopg.connect(self.connection_string, row_factory=dict_row)
        return self.connection

    def create_tables(self):
        with self.get_connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    CREATE TABLE IF NOT EXISTS jobs (
                        id SERIAL PRIMARY KEY,
                        hash VARCHAR(80) NOT NULL,
                        job VARCHAR(30) NOT NULL,
                        ci_pipeline_id INTEGER NOT NULL,
                        metadata JSON NOT NULL,
                        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP);

                    ALTER TABLE jobs DROP CONSTRAINT IF EXISTS no_duplicates;

                    ALTER TABLE jobs ADD CONSTRAINT no_duplicates UNIQUE (hash, job);

                    CREATE OR REPLACE VIEW pipelines_overview AS
                        SELECT p1.ci_pipeline_id,
                            json_agg(DISTINCT split_part((p1.job)::text, '_'::text, 2)) AS data_models,
                            ( SELECT json_object_agg(k.split, k.job) AS json_object_agg
                                FROM ( SELECT DISTINCT split_part((p2.metadata ->> 'ci_job_name'::text), '_'::text, 2) AS split,
                                            jsonb_agg(p2.job) AS job
                                        FROM jobs p2
                                        WHERE (p2.ci_pipeline_id = 725)
                                        GROUP BY (split_part((p2.metadata ->> 'ci_job_name'::text), '_'::text, 2))) k) AS jobs_executed,
                            ( SELECT json_object_agg(split_part((p2.job)::text, '_'::text, 2), json_build_object('hash', p2.hash, 'hex_sha', (p2.metadata -> 'hex_sha'::text), 'data_location', (p2.metadata -> 'data_location'::text), 'ci_job_name', (p2.metadata -> 'ci_job_name'::text), 'inputs', (p2.metadata -> 'inputs'::text), 'outputs', (p2.metadata -> 'outputs'::text))) AS json_object_agg
                                FROM jobs p2
                                WHERE ((p2.ci_pipeline_id = p1.ci_pipeline_id) AND ((p2.job)::text ~~ 'download_%'::text) AND ((p2.job)::text <> 'download_historical'::text))) AS downloaded_datasets,
                            ( SELECT json_object_agg(split_part((p2.job)::text, '_'::text, 2), json_build_object('hash', p2.hash, 'hex_sha', (p2.metadata -> 'hex_sha'::text), 'data_location', (p2.metadata -> 'data_location'::text), 'ci_job_name', (p2.metadata -> 'ci_job_name'::text), 'outputs', (p2.metadata -> 'outputs'::text))) AS json_object_agg
                                FROM jobs p2
                                WHERE ((p2.ci_pipeline_id = p1.ci_pipeline_id) AND ((p2.job)::text ~~ 'process%'::text))) AS bias_adjusted_datasets
                        FROM jobs p1
                        GROUP BY p1.ci_pipeline_id;
                    
                    CREATE TABLE IF NOT EXISTS climate_models (
                        id SERIAL PRIMARY KEY,
                        filename character varying(1000) NOT NULL,
                        search_filters jsonb NOT NULL,
                        scenarios json NOT NULL,
                        created_at timestamp(6) without time zone DEFAULT now() NOT NULL,
                        version integer DEFAULT 1 NOT NULL
                    );

                    ALTER TABLE climate_models DROP CONSTRAINT IF EXISTS no_duplicates_cm;

                    ALTER TABLE climate_models ADD CONSTRAINT no_duplicates_cm UNIQUE (search_filters, version);
                """
                )
                conn.commit()

    def find_job(
        self,
        job_hash: str,
        job: str,
    ):
        with self.get_connection() as conn:
            with conn.cursor() as cur:
                where_conditions = []
                where_conditions.append(f"hash = '{job_hash}'")
                where_conditions.append(f"job = '{job}'")

                query = "SELECT * FROM jobs"
                first = True
                for w in where_conditions:
                    query += self.__add_where(w, first=first)
                    first = False
                query += " ORDER BY created_at DESC LIMIT 1"

                print("EXECUTING QUERY", query)
                cur.execute(query)
                return cur.fetchone()

    def save_job(
        self,
        job_hash: str,
        job: str,
        ci_pipeline_id: int,
        metadata: dict,
    ):
        with self.get_connection() as conn:
            with conn.cursor() as cur:
                metadata_json = json.dumps(metadata)
                cur.execute(
                    "INSERT INTO jobs (hash, job, ci_pipeline_id, metadata) VALUES (%s, %s, %s, %s)",
                    (job_hash, job, ci_pipeline_id, metadata_json),
                )
                conn.commit()

    def get_merge_historical_hash(
        self,
        ci_pipeline_id: int,
    ):
        with self.get_connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    "SELECT hash FROM jobs WHERE job = 'merge_historical' AND ci_pipeline_id = %s",
                    (ci_pipeline_id,),
                )
                return cur.fetchone()["hash"]

    def get_all_download_hashes(
        self,
        ci_pipeline_id: int,
    ):
        with self.get_connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    "SELECT hash FROM jobs WHERE job LIKE 'download%%' AND ci_pipeline_id = %s",
                    (ci_pipeline_id,),
                )
                all_download_hashes = []
                for record in cur.fetchall():
                    all_download_hashes.append(record["hash"])
                return all_download_hashes

    def update_ci_pipeline_id(
        self,
        ci_pipeline_id: int,
        job_hash: str,
    ):
        with self.get_connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    "UPDATE jobs SET ci_pipeline_id = %s WHERE hash = %s",
                    (
                        ci_pipeline_id,
                        job_hash,
                    ),
                )
                conn.commit()

    def __add_where(self, where: str, first: bool) -> str:
        if first:
            return " WHERE " + where
        return " AND " + where
