from abc import abstractmethod
from pydoc import locate
from typing import Optional


class Database:
    connection_string: str = ""
    connection = None

    def __init__(self, connection_string: str):
        self.connection = None
        self.connection_string = connection_string

    @abstractmethod
    def get_connection(self):
        raise NotImplementedError

    @abstractmethod
    def create_tables(self):
        raise NotImplementedError

    @abstractmethod
    def find_job(
        self,
        job_hash: str,
        job: str,
    ):
        raise NotImplementedError

    @abstractmethod
    def save_job(
        self,
        job_hash: str,
        job: str,
        ci_pipeline_id: int,
        metadata: dict,
    ):
        raise NotImplementedError

    @abstractmethod
    def get_merge_historical_hash(
        self,
        ci_pipeline_id: int,
    ):
        raise NotImplementedError

    @abstractmethod
    def update_ci_pipeline_id(
        self,
        ci_pipeline_id: int,
        job_hash: str,
    ):
        raise NotImplementedError

    @abstractmethod
    def get_all_download_hashes(
        self,
        ci_pipeline_id: int,
    ):
        raise NotImplementedError


def database_factory(class_name: Optional[str], db_connection_url: str) -> Database:
    db_class = locate("db." + (class_name or "database_psycopg.DatabasePsycopg"))
    return db_class(db_connection_url)
