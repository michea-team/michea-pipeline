# michea-pipeline

Project home: https://gitlab.com/michea-team/michea-pipeline

> This is preliminary documentation that may imply features that are currently at the planning or development stage

[[_TOC_]]

## Abstract

**Michea** is a platform for batch automated statistical processing of climate models, which uses the [Gitlab CI (Continuous Integration)](https://docs.gitlab.com/ee/ci/introduction/index.html#continuous-integration) as scheduler.

It allows to:

- launch asynchronous pipelines in the cloud or on premise that download climate models, validate and pre-process them, perform the statistical elaborations and the reporting

- keep track of all elaborations

- browse and download the produced result files

- efficiently cache intermediate files so that they must not be downloaded or produced over and over again if similar elaborations are requested

## Architecture

The main components of **michea** are:

1. the public software development platform [gitlab.com](https://about.gitlab.com) or a self-hosted instance of the underlying Community Edition software

2. a single virtual machine in the cloud or on-premise which provides storage and processing power in one place, which must be always on and configured to act as a [Gitlab runner](https://docs.gitlab.com/runner/#gitlab-runner) to the above

3. the main **michea-pipeline** repository, which holds both the Python / R code used to perform the elaborations as well as the `.gitlab-ci.yml` file that defines the pipelines

4. a PostgreSQL database that is used to associate the result files of each job in each pipeline with:

   - the source code version
  
   - the input data and / or the upstream jobs result files

4. a simple user interface (see the separate repo [michea-UI](https://gitlab.com/michea-team/michea-ui)) that allows to:

   - interactively define the source climate models, the geographical domain, the required statistical elaboration and climate indices, and download a ready-to-use `.gitlab-ci.yml` file
  
   - browse and download the intermediate and result files, linking them back to the processing pipelines that produced them so that they can be traced back to the elaborations that produced them.

## Using vscode

Get Visual Studio Code from: https://code.visualstudio.com

Install python3 and mypy systemwide:

    sudo apt install python3 mypy

Create the virtual environment using the command line:

    python3 -m venv .venv
    source .venv/bin/activate
    pip3 install -r requirements.txt 

Open the Command Palette (Ctrl+Shift+P), start typing the _Python: Select Interpreter_ command to search, and then select it; set the interpreter to `.venv/bin/python`, see: https://code.visualstudio.com/docs/python/environments

When you open the michea-pipeline workspace for the first time, install the two extensions (Python and Mypy) that vscode recommends; if you dismissed the recommendations, you can find them at any time in the extensions marketplace:

- official _Python_ extension (`ms-python.python`)
- _Type checking for Python using mypy_ (`matangover.mypy`).

## Getting started

The following outlines the steps a user may follow, provided she is authorized to access the [canonical michea-pipeline repository](https://gitlab.com/michea-team/michea-pipeline) with [developer or maintainer roles](https://docs.gitlab.com/ee/user/permissions.html), and she can access via HTTP the associated on-premise runner (in the following, `runner.example.com`).

If you do not have access to these resources, please look at the _Self host_ section below.

### Prerequisites

All the software you need locally is: [git](https://git-scm.com), the `ssh` OpenSSH client and a browser; the suggested way to install those on Windows is via the [Windows Subsystem for Linux (WSL)](https://learn.microsoft.com/en-us/windows/wsl/install).

You already have an authorized account on gitlab.com and you have [configured the SSH key for secure access to GitLab](https://docs.gitlab.com/ee/user/ssh.html).

### Launching a pipeline

Clone the michea-pipeline repository with SSH:

```sh
git clone git@gitlab.com:michea-team/michea-pipeline.git
```

then change directory and create your own branch:

```sh
cd michea-pipeline
git switch -c wip/your-username
```

Open up the michea UI on the runner at https://runner.example.com/michea/input.html and define the parameters of your run.

When your are done, click on the _Download `.gitlab-ci.yml` file_, and save it locally as `michea-pipeline/.gitlab-ci.yml`, overwriting the existing file.

To actually launch che pipeline, commit the file with a descriptive message and push your branch:

```sh
git commit -am 'average rainfall for Lombardy in 2021-2040 from the 8 reference GCMs, with 2 km DS and BA against Arcis 1996-2015 data'
git push
```

### Watching the pipeline progress

Visit your branch homepage: https://gitlab.com/michea-team/michea-pipeline/-/tree/wip/your-username

In the top of the page your commit ("_average rainfall for Lombardy in 2021-2040 from the 8 reference GCMs, with 2 km DS and BA against Arcis 1996-2015 data_") is highlighted, at the right it should show the round blue "_status running_" icon (![status-running](images/_icon_status_running.svg "running status icon")), as soon as it successfully completes it will show the green "_status success_" icon (![status-success](images/_icon_status_success.svg "success status icon")).

If the pipeline fails(![status-error](images/_icon_status_error.svg "success status icon")), it can be restarted using the retry button located next to the pipeline log.

### Looking at results

Go back to the michea UI on the runner at https://runner.example.com/michea/results.html to browse and download the intermediate and result files for your pipelines and those of your colleagues.

From there you can also click on the "source branch" and "pipeline" links, which will bing you back to gitlab.com.

## Self host

### Prerequisites

Have at your disposal a virtual machine in the cloud or on-premise with at least **X** CPUs, **Y** GB RAM, **W** GB SSD storage and **Z** GB non-SSD storage.

The gitlab-runner service does not require opening special ports, it just requires outbound access to the public internet over the HTTPS protocol.

If the virtual machine operates behind a proxy there are special configuration requirements: https://docs.gitlab.com/runner/configuration/proxy.html,

Fork the michea-pipeline repository into your namespace, i.e. https://gitlab.com/my-namespace/michea-pipeline.

### Docker set-up michea-UI

See: https://gitlab.com/michea-team/michea-ui#docker-set-up

Once you have completed the setup, make sure that in the "_Runners_" section under **CI/CD Settings** of your michea-pipeline repository (i.e. https://gitlab.com/my-namespace/michea-pipeline/-/settings/ci_cd), the runner is configured and accepting jobs.

### Configure

Create a `/var/michea-storage` directory, preferably on non-SSD storage, where the docker containers spun off by the gitlab runner will save the intermediate and result files:

    sudo mkdir /var/michea-storage

(the files will be created by the `root` user so there's no need to change the owner of that dir).

This path must be set as `JOB_DATA_LOCATION` in the `.env` file.

### Secrets

You need to add the following secrets in the "_Variables_" section under **CI/CD Settings** of your michea-pipeline repository (i.e. https://gitlab.com/my-namespace/michea-pipeline/-/settings/ci_cd) as [protected, masked and expanded Gitlab CI variables](https://docs.gitlab.com/ee/ci/variables/#cicd-variable-security):

1. `DATABASE_URL`: a valid [PostgreSQL Connection URI](https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING-URIS); make sure the containers started by the runner are able to reach the `host:port`;

2. the [ESGF-CORDEX openid credentials](https://esgf-node.ipsl.upmc.fr/user/add/) in the variables `ESGF_OPENID` (for example: `https://esgf-node.ipsl.upmc.fr/esgf-idp/openid/myusername`) and `ESGF_PASSWORD`.

## Running locally

During development it is handy to run the jobs one by one locally.

For that you can use the provided compose files as follows:

1. create the empty temporary dir:

        sudo rm -rf /tmp/michea-storage
        mkdir /tmp/michea-storage

2. create the initially empty `job.env`:

        rm job.env
        touch job.env

3. copy the `*.env.example` files to `*.env`:

        cp download_esgf_cordex_dataset_historical.env.example download_esgf_cordex_dataset_historical.env
        cp download_esgf_cordex_wget_historical.env.example download_esgf_cordex_wget_historical.env
        cp crop_historical.env.example crop_historical.env
        cp merge_historical.env.example merge_historical.env

        cp download_esgf_cordex_dataset.env.example download_esgf_cordex_dataset.env
        cp download_esgf_cordex_wget.env.example download_esgf_cordex_wget.env
        cp crop.env.example crop.env
        cp merge.env.example merge.env

        cp process.env.example process.env
        cp clean_up.env.example clean_up.env

        cp calculate_index.env.example calculate_index.env

4. modify `download_esgf_cordex_dataset.env` and `download_esgf_cordex_dataset_historical.env` setting the variables `ESGF_OPENID` and `ESGF_PASSWORD` to the actual openid and password

5. launch the jobs as follows:

        IMAGE=base ENV=download_esgf_cordex_wget_historical.env docker-compose up
        # stop the job (CTRL+C)
        IMAGE=esgf_cordex ENV=download_esgf_cordex_dataset_historical.env docker-compose up
        # stop the job (CTRL+C)
        IMAGE=cdo ENV=crop_historical.env docker-compose up
        # stop the job (CTRL+C)
        IMAGE=cdo ENV=merge_historical.env docker-compose up

        IMAGE=base ENV=download_esgf_cordex_wget.env docker-compose up
        # stop the job (CTRL+C)
        IMAGE=esgf_cordex ENV=download_esgf_cordex_dataset.env docker-compose up
        # stop the job (CTRL+C)
        IMAGE=cdo ENV=crop.env docker-compose up
        # stop the job (CTRL+C)
        IMAGE=cdo ENV=merge.env docker-compose up
        # stop the job (CTRL+C)

        IMAGE=r ENV=process.env docker-compose up
        # stop the job (CTRL+C)
        IMAGE=base ENV=clean_up.env docker-compose up
        # stop the job (CTRL+C)

        IMAGE=calculate_index ENV=calculate_index.env docker-compose up
        # stop the job (CTRL+C)

6. in another terminal, inspect DB content:

        docker-compose exec -T postgres psql michea -U michea -c 'SELECT * FROM jobs'

    you should see:

    | id |                               hash                               |             job              |                                                                                                                                                                                                                                                                                metadata                                                                                                                                                                                                                                                |         created_at         |
    |----|------------------------------------------------------------------|------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------|
    | 1  | ccee24b43e6092ef2da0917e56ffae236b2cf73529f427d26287c3857e6d7fcd | download_esgf_cordex_wget    | `{"hex_sha": "687824539d471eff0299408b14c11507d6341843", "data_location": "/var/michea-storage", "inputs": ["output.EUR-11.CLMcom.ICHEC-EC-EARTH.rcp45.r12i1p1.CCLM4-8-17.v1.day.pr"], "outputs": ["wget.sh"], "version": "1", "ci_job_stage": "download_script", "ci_job_name": "download_esgf_cordex_wget", "ci_previous_job_hash": null}`                                                                                                                                                                                             | 2023-03-06 12:18:25.363521 |
    | 2  | 7236b10ae69489ed28c198f8fe4d86bb7fd235ec42a5f679f29d4a58ff5ee242 | download_esgf_cordex_dataset | `{"hex_sha": "687824539d471eff0299408b14c11507d6341843", "data_location": "/var/michea-storage", "inputs": ["wget.sh"], "outputs": ["pr_EUR-11_ICHEC-EC-EARTH_rcp45_r12i1p1_CLMcom-CCLM4-8-17_v1_day_20060101-20101231.nc", ...], "version": "1", "ci_job_stage": "download_dataset", "ci_job_name": "download_esgf_cordex_dataset", "ci_previous_job_hash": "ccee24b43e6092ef2da0917e56ffae236b2cf73529f427d26287c3857e6d7fcd", "ESGF_OPENID": "https://esgf-node.ipsl.upmc.fr/esgf-idp/openid/username", "ESGF_PASSWORD": "password"}` | 2023-03-06 12:19:44.243104 |
    | 3  | 8dacccf315b9048bf5e57cdc80c46d9790edf3ba8531488d2c65cd43e9911288 | merge_files                  | `{"hex_sha": "4dcc31a88693adc4c0606586bd77fc04ea1097d3", "data_location": "/var/michea-storage", "inputs": ["pr_EUR-11_ICHEC-EC-EARTH_rcp45_r12i1p1_CLMcom-CCLM4-8-17_v1_day_20060101-20101231.nc", ...], "outputs": ["dataset.nc"], "version": "1", "ci_job_stage": "data_preparation", "ci_job_name": "merge_files", "ci_previous_job_hash": "7236b10ae69489ed28c198f8fe4d86bb7fd235ec42a5f679f29d4a58ff5ee242"}`                                                                                                                      | 2023-03-06 12:20:08.038819 |

7. to clean up the database:

        docker-compose exec postgres psql michea -U michea -c 'DELETE FROM jobs'

8. back to the first terminal, stop the last job (CTRL+C).
