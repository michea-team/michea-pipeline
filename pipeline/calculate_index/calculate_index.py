
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import pandas as pd
import xarray as xr
import xclim

from typing import List, Optional

from pipeline.pipeline_handler_factory import Handler, HandlerResult


class CalculateIndex(Handler):
    def fill_metadata(self, metadata: dict) -> dict:
        for var in [
            "CLIM_VAR",
            "CLIM_TRAINING_VAR",
            "CLIMATE_INDEX"
        ]:
            metadata[var] = os.getenv(var)
        #  index = metadata["CLIMATE_INDEX"]
        # TODO use f string to pararametrix the index
        metadata["outputs"] = [
            "Avg_R99pTOT_21-40.png",
            "Avg_R99pTOT_41-60.png", 
            "Avg_R99pTOT_training_dataset.png",
            "Avg_R99pTOT_21-40.nc",
            "Avg_R99pTOT_41-60.nc", 
            "Avg_R99pTOT_training_dataset.nc",
            ]
        return metadata

    def handle(
        self,
        volume_path: str,
        job_inputs: List[str],
        job_outputs: List[str],
        job_metadata: dict,
        job_data_location: str,
        ci_pipeline_id: int,
        previous_job_data_location: Optional[str],
    ) -> HandlerResult:
        dataset_name =  os.getenv("DATA_LOCATION") + "/" + os.getenv("EXECUTED_JOB_HASH") + "/" + job_inputs[0]
        clim_var = job_metadata["CLIM_VAR"]
        dataset_training_name = os.getenv("DATA_LOCATION") + "/" + "file_utili/ARCIS_GG_1961-2015_LOMB.nc"
        clim_training_var = job_metadata["CLIM_TRAINING_VAR"]
        output_dir = volume_path + '/' + job_data_location

        try:       
            if job_metadata["CLIMATE_INDEX"] == 'R99pTOT':
                self.__R99pTOT(
                  dataset_name = dataset_name,
                  dataset_training_name = dataset_training_name,
                  clim_var = clim_var,
                  clim_training_var = clim_training_var,
                  output_dir = output_dir
              )
        except Exception as e:
            print(f"An error occurred: {e}")
            return HandlerResult(success=False)

        return HandlerResult(success=True)

    def __R99pTOT(
        self,
        dataset_name: str,
        dataset_training_name: str,
        clim_var: str,
        clim_training_var: str,
        output_dir: str
    ) -> bool:
        dataset = xr.open_dataset(dataset_name)
        dataset_training = xr.open_dataset(dataset_training_name)
        # get variable from datasets
        dataset_var = dataset[clim_var]
        dataset_var.attrs['units'] = 'mm d-1'
        dataset_training_var = dataset_training[clim_training_var]

        # calculate quantile
        dim = ('time')
        q = dataset_training_var.quantile(0.99, dim, keep_attrs=True)

        # using pandas to get the 2 ranges of 20 years
        date_range_21_40 = pd.date_range('2021-01-01', '2040-12-31', freq='D')
        date_range_41_60 = pd.date_range('2041-01-01', '2060-12-31', freq='D')

        # split the dataset into 2 ranges of 20 years
        dataset_var_21_40 = dataset_var.sel(time=date_range_21_40)
        dataset_var_41_60 = dataset_var.sel(time=date_range_41_60)

        # calculate indices for time range
        dataset_index_21_40 = xclim.indicators.icclim.R99pTOT(pr=dataset_var_21_40, pr_per=q)
        dataset_index_41_60 = xclim.indicators.icclim.R99pTOT(pr=dataset_var_41_60, pr_per=q)
        dataset_training_index = xclim.indicators.icclim.R99pTOT(pr=dataset_training_var, pr_per=q)

        # mean and standard deviation
        index_mean_21_40 = dataset_index_21_40.mean(dim='time')
        index_std_dev_21_40 = dataset_index_21_40.std(dim='time')

        index_mean_41_60 = dataset_index_41_60.mean(dim='time')
        index_std_dev_41_60 = dataset_index_41_60.std(dim='time')

        index_mean_training = dataset_training_index.mean(dim='time')
        index_std_dev_training = dataset_training_index.std(dim='time')

        # draw graphs about means and std_devs
        to_print = {
          "Avg_R99pTOT_21-40": index_mean_21_40,
          "Avg_R99pTOT_41-60": index_mean_41_60,
          "Avg_R99pTOT_training_dataset": index_mean_training
        }

        # get max and min of all datasets
        dims_to_print = ('lat', 'lon')
        maxs = max(index_mean_21_40.max(dims_to_print), index_mean_41_60.max(dims_to_print), index_mean_training.max(dims_to_print))
        mins = min(index_mean_21_40.min(dims_to_print), index_mean_41_60.min(dims_to_print), index_mean_training.min(dims_to_print))
        levels = np.linspace(mins, maxs, 10)

        for k, i in to_print.items():

            output_path = output_dir + '/' + k

            # save datasets in the storage
            i.to_netcdf(path=output_path + '.nc')

            # all lat and lon values but only index 0 for time
            contour_values = i[:].values

            # lat e lon values
            lat_contour = i['lat'].values
            lon_contour = i['lon'].values

            # contour plot
            plt.contourf(lon_contour, lat_contour, contour_values, levels)
            plt.axis('on')
            plt.colorbar()
            plt.xlabel('Lon')
            plt.ylabel('Lat')
            plt.title(k)
            plt.savefig(output_path + '.png')
            plt.clf()
