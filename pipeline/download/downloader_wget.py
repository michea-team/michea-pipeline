from typing import List, Optional

import wget as wget

from pipeline.pipeline_handler_factory import Handler, HandlerResult


class DownloaderWget(Handler):
    def handle(
        self,
        volume_path: str,
        job_inputs: List[str],
        job_outputs: List[str],
        job_metadata: dict,
        job_data_location: str,
        ci_pipeline_id: int,
        previous_job_data_location: Optional[str],
    ) -> HandlerResult:
        downloaded = []

        for i, file in enumerate(job_inputs):
            downloaded_file = wget.download(
                file, volume_path + "/" + job_data_location + "/" + job_inputs[i]
            )

            downloaded.append(
                {
                    "requested_file": file,
                    "downloaded_file": downloaded_file,
                    "path": volume_path + "/" + job_data_location,
                }
            )

        return HandlerResult(success=True, data=downloaded)
