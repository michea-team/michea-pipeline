import glob
import os
import subprocess
from typing import Iterable, List, Optional

from pipeline.pipeline_handler_factory import Handler, HandlerResult


class DownloaderEsgfCordexDataset(Handler):

    def fill_metadata(self, metadata: dict) -> dict:
        for var in ["ESGF_OPENID", "ESGF_PASSWORD"]:
            metadata[var] = os.getenv(var)
        return metadata

    def handle(
            self,
            volume_path: str,
            job_inputs: List[str],
            job_outputs: List[str],
            job_metadata: dict,
            job_data_location: str,
            ci_pipeline_id: int,
            previous_job_data_location: Optional[str]
    ) -> HandlerResult:
        if previous_job_data_location is None:
            raise ValueError('previous_job_data_location not found. The env variable EXECUTED_JOB_HASH may have not '
                             'been set by the previous job.')

        self.__download(
            wget_script=volume_path + "/" + previous_job_data_location + "/" + job_inputs[0],
            destination=volume_path + "/" + job_data_location
        )

        downloaded = self.__check_files_downloaded(
            expected_files=job_outputs,
            files_folder=volume_path + "/" + job_data_location,
            download_status_cache_file=volume_path + "/" + job_data_location + "/" + ".wget.sh.status"
        )

        if not downloaded:
            print("DATASET PARTS WERE NOT FULLY DOWNLOADED")

        return HandlerResult(
            success=downloaded,
        )

    def __download(
            self,
            wget_script: str,
            destination: str,
    ) -> dict:
        openid = os.getenv("ESGF_OPENID")
        password = os.getenv("ESGF_PASSWORD")
        # save current directory
        project_root = os.getcwd()
        os.chdir(destination)
        subprocess.run(
            args=[wget_script, "-H"],
            input=bytes(openid, "ascii") + b"\n" + bytes(password, "ascii"),
            stdout=subprocess.PIPE,
        )
        # return to initial directory. main.py saves a job.env file in the root directory of the project.
        os.chdir(project_root)
        print("done")

    def __check_files_downloaded(self, expected_files: List[str], files_folder: str, download_status_cache_file: str) -> bool:
        if not os.path.isfile(download_status_cache_file):
            return False

        cache_file_content = open(download_status_cache_file, 'r')
        downloaded_files = cache_file_content.readlines()

        for downloaded in downloaded_files:
            filename = downloaded.split()[0].replace("'", "")
            if filename[0] == "#":
                continue
            if filename not in expected_files:
                return False

        list_of_files = filter(os.path.isfile, glob.glob(files_folder + '/*.nc'))
        for file_path in list_of_files:
            if os.stat(file_path).st_size == 0:
                return False

        return True






