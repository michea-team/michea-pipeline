import os
from typing import List, Optional
from urllib.parse import urlencode

import wget as wget

from pipeline.pipeline_handler_factory import Handler, HandlerResult


class DownloaderEsgfCordexWget(Handler):

    def fill_metadata(self, metadata: dict) -> dict:
        keys = os.getenv("KEYS", "").split()
        fields = os.getenv("FIELDS", "").split()
        metadata["search_filters"] = dict(zip(keys, fields))
        return metadata

    def handle(
        self,
        volume_path: str,
        job_inputs: List[str],
        job_outputs: List[str],
        job_metadata: dict,
        job_data_location: str,
        ci_pipeline_id: int,
        previous_job_data_location: Optional[str],
    ) -> HandlerResult:
        downloaded_data = self.__download(
            search_filters = job_metadata["search_filters"],
            output_filename=job_outputs[0],
            destination=volume_path + "/" + job_data_location,
        )

        return HandlerResult(success=True, data=downloaded_data)

    def __download(
        self,
        search_filters: dict,
        output_filename: str,
        destination: str,
    ) -> List[dict]:

        downloaded = []
        query_string_params = []
        for filter, value in search_filters.items():
            print("filter", filter)
            print("value", value)
            query_string_params.append((filter, value))

        url = "https://esgf-data.dkrz.de/esg-search/wget?" + urlencode(
            query_string_params
        )

        print(url)
        downloaded_file = wget.download(url, destination + "/" + output_filename)
        os.chmod(destination + "/" + output_filename, 0o744)
        downloaded.append(
            {
                "requested_file": output_filename,
                "downloaded_file": downloaded_file,
                "path": destination,
            }
        )

        return downloaded
