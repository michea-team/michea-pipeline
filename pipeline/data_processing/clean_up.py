import os
from typing import List, Optional

from db.database import database_factory
from pipeline.pipeline_handler_factory import Handler, HandlerResult


class CleanUp(Handler):
    def fill_metadata(self, metadata: dict) -> dict:
        metadata["CURL_CI_PIPELINE_ID"] = os.getenv("CURL_CI_PIPELINE_ID")
        return metadata
    
    def handle(
        self,
        volume_path: str,
        job_inputs: List[str],
        job_outputs: List[str],
        job_metadata: dict,
        job_data_location: str,
        ci_pipeline_id: int,
        previous_job_data_location: Optional[str],
    ) -> HandlerResult:
        db = database_factory(os.getenv("DATABASE"), os.getenv("DATABASE_URL"))
        if os.getenv("CURL_CI_PIPELINE_ID") == None:
            curl_ci_pipeline_id = ci_pipeline_id
        else:
            curl_ci_pipeline_id = job_metadata["CURL_CI_PIPELINE_ID"]

        download_hashes = db.get_all_download_hashes(curl_ci_pipeline_id)

        clean_data = self.__clean(
            download_hashes=download_hashes,
            job_data_location=job_data_location,
            curl_ci_pipeline_id=curl_ci_pipeline_id,
        )
        return HandlerResult(success=clean_data)

    def __clean(
        self,
        download_hashes: List[str],
        job_data_location: str,
        curl_ci_pipeline_id: int,
    )-> bool:
        
        data_location = os.getenv("DATA_LOCATION")

        # since is useless, remove the clean_up_job folder 
        os.system(f"rm -rf {data_location}/{job_data_location}/")

        if download_hashes == []:
            print("NO DOWNLOAD HASHED ARE FOUND FOR CI PIPELINE ID = " + str(curl_ci_pipeline_id))
            return False

        for hash in download_hashes:
            print(f"Folder {hash} and its contents will be removed")
            exit_code = os.system(f"rm -rf {data_location}/{hash}/")
            if exit_code != 0:
                return exit_code == 0
        
        return exit_code == 0
 

