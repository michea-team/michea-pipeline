import os
from typing import List, Optional

from db.database import database_factory
from pipeline.pipeline_handler_factory import Handler, HandlerResult


class ExecuteR(Handler):
    def fill_metadata(self, metadata: dict) -> dict:
        for var in [
            "R_FILE",
            "YTRAI_START",
            "YTRAI_END",
            "YPROJ_START",
            "YPROJ_END",
            "TRAINING_DATASET"
        ]:
            metadata[var] = os.getenv(var)
        return metadata

    def handle(
        self,
        volume_path: str,
        job_inputs: List[str],
        job_outputs: List[str],
        job_metadata: dict,
        job_data_location: str,
        ci_pipeline_id: int,
        previous_job_data_location: Optional[str],
    ) -> HandlerResult:
        if previous_job_data_location is None:
            raise ValueError(
                "previous_job_data_location not found. The env variable EXECUTED_JOB_HASH may have not "
                "been set by the previous job."
            )

        ci_project_dir = os.getenv("CI_PROJECT_DIR")
        r_file = job_metadata["R_FILE"]

        db = database_factory(os.getenv("DATABASE"), os.getenv("DATABASE_URL"))
        merge_historical_hash = db.get_merge_historical_hash(ci_pipeline_id)
        os.environ["MERGE_HISTORICAL_HASH"] = merge_historical_hash
        print("MERGE_HISTORICAL_HASH", os.getenv("MERGE_HISTORICAL_HASH"))
        os.environ["JOB_DATA_LOCATION"] = job_data_location

        success = self.__execute(
            ci_project_dir=ci_project_dir,
            r_file=r_file,
        )
        return HandlerResult(success=success)

    def __execute(self, ci_project_dir: str, r_file: str) -> bool:
        exit_code = os.system(f"/usr/bin/R --no-save < {ci_project_dir}/pipeline/data_processing/{r_file}")
        return exit_code == 0
