#libero eventuale memoria impegnata
rm(list=ls())

#librerie
library(stringr) # r-cran-stringr
#library(climate4R.climdex) #Indici climatici
library(raster) # r-cran-raster
C4R <- list("loadeR","transformeR","downscaleR","visualizeR") # removed from the list "climate4R.climdex"
lapply(C4R,require,character.only = TRUE)
library(sf) # r-cran-sf
library(ncdf4) # r-cran-ncdf4
library(loadeR.2nc) # Error in library(loadeR.2nc) : there is no package called ‘loadeR.2nc’
library(rasterVis) # Error in library(rasterVis) : there is no package called ‘rasterVis’
library(latticeExtra) # r-cran-latticeextra
library(lubridate) # r-cran-lubridate

# Periodi
#periodo di training
ytrai_start = Sys.getenv("YTRAI_START")
ytrai_end = Sys.getenv("YTRAI_END")
ytrai = ytrai_start:ytrai_end   # per CMIP6: 1986-2015 ?
#periodo di proiezione
yproj_start = Sys.getenv("YPROJ_START")
yproj_end = Sys.getenv("YPROJ_END")
yproj = yproj_start:yproj_end   # per CMIP6: 1986-2015 ?  # TODO: 2006:2100, per CMIP6: 2016-2100 ?

# lettura dei dati ARCIS per eseguire il downscaling
training_dataset <- paste(Sys.getenv("DATA_LOCATION"), Sys.getenv("TRAINING_DATASET"), sep = "/")

dict_training_dataset <- paste(Sys.getenv("DATA_LOCATION"), Sys.getenv("TRAINING_DATASET_DICT"), sep = "/")

if (Sys.getenv("TRAINING_DATASET") == "file_utili/ARCIS_GG_1961-2015_LOMB.nc") {
  clim_var <- "pr"
  precipitation <- TRUE
}

if (Sys.getenv("TRAINING_DATASET") == "file_utili/era5-downscaled-over-italy-VHR-REA_IT_1989_2020_daily_tas_LOMB_remap2.nc") {
  clim_var <- "tas"
  precipitation <- FALSE
}

training_dataset_trai <- loadGridData(training_dataset,
                              var = clim_var,
                              season =1:12,
                              years = ytrai,          
                              dictionary = dict_training_dataset
                              )

#armonizzo formato date
        
#str(training_dataset_trai$Dates$start)
training_dataset_trai$Dates$start <- as.Date(training_dataset_trai$Dates$start)
#str(training_dataset_trai$Dates$start)
training_dataset_trai$Dates$start <- ymd(training_dataset_trai$Dates$start)
#str(training_dataset_trai$Dates$start)
training_dataset_trai$Dates$start <- as.character(training_dataset_trai$Dates$start)
#str(training_dataset_trai$Dates$start)    

#str(training_dataset_trai$Dates$end)
training_dataset_trai$Dates$end <- as.Date(training_dataset_trai$Dates$end)
#str(training_dataset_trai$Dates$end)
training_dataset_trai$Dates$end <- ymd(training_dataset_trai$Dates$end)
#str(training_dataset_trai$Dates$end)
training_dataset_trai$Dates$end <- as.character(training_dataset_trai$Dates$end)
#str(training_dataset_trai$Dates$end)

gc()

# dizionario
dict_dataset <- paste(Sys.getenv("DATA_LOCATION"), Sys.getenv("DATASET_DICT"), sep = "/")

#lettura del modello nel periodo di training
dataset = paste(Sys.getenv("DATA_LOCATION"), Sys.getenv("MERGE_HISTORICAL_HASH"), Sys.getenv("INPUTS_HISTORICAL_DATASET"), sep = "/")
cat("PATH HISTORICAL DATASET == ", dataset, "\n")
dataset_trai <- loadGridData(dataset = dataset,
                             var = clim_var,
                             season = 1:12,
                             years = ytrai,          
                             dictionary = dict_dataset
                             )

#armonizzo formato date
#str(dataset_trai$Dates$start)
dataset_trai$Dates$start <- as.Date(dataset_trai$Dates$start)
#str(dataset_trai$Dates$start)
dataset_trai$Dates$start <- ymd(dataset_trai$Dates$start)
#str(dataset_trai$Dates$start)
dataset_trai$Dates$start <- as.character(dataset_trai$Dates$start)
#str(dataset_trai$Dates$start)

#str(dataset_trai$Dates$end)
dataset_trai$Dates$end <- as.Date(dataset_trai$Dates$end)
#str(dataset_trai$Dates$end)
dataset_trai$Dates$end <- ymd(dataset_trai$Dates$end)
#str(dataset_trai$Dates$end)
dataset_trai$Dates$end <- as.character(dataset_trai$Dates$end)
#str(dataset_trai$Dates$end)

#lettura del modello nel periodo di proiezione
dataset = paste(Sys.getenv("DATA_LOCATION") ,Sys.getenv("EXECUTED_JOB_HASH"), Sys.getenv("INPUTS_DATASET"), sep = "/")
cat("PATH FORECAST DATASET == ", dataset, "\n")
dataset_proj <- loadGridData(dataset = dataset,
                             var = clim_var,
                             season = 1:12,
                             years = yproj,          
                             dictionary = dict_dataset
                             )

#armonizzo formato date
#str(dataset_proj$Dates$start)
dataset_proj$Dates$start <- as.Date(dataset_proj$Dates$start)
#str(dataset_proj$Dates$start)
dataset_proj$Dates$start <- ymd(dataset_proj$Dates$start)
#str(dataset_proj$Dates$start)
dataset_proj$Dates$start <- as.character(dataset_proj$Dates$start)
#str(dataset_proj$Dates$start)

#str(dataset_proj$Dates$end)
dataset_proj$Dates$end <- as.Date(dataset_proj$Dates$end)
#str(dataset_proj$Dates$end)
dataset_proj$Dates$end <- ymd(dataset_proj$Dates$end)
#str(dataset_proj$Dates$end)
dataset_proj$Dates$end <- as.character(dataset_proj$Dates$end)
#str(dataset_proj$Dates$end)

############################################################
############          isimip 3 BASD - pay attention to isimip.args field to be fullfilled carefully for every ECV 
############################################################
       
SEED <- 1 
SEED <- as.integer(SEED)                          
dataset_proj.BASD <- biasCorrection(training_dataset_trai, 
                                    dataset_trai, 
                                    dataset_proj, 
                                    precipitation=precipitation,
                                    wet.threshold=0.1,
                                    method="isimip3", 
                                    join.members = FALSE,
                                    isimip3.args = list(dates=c(ytrai,ytrai,yproj), 
                                                    lower_bound=c(0), 
                                                    lower_threshold=c(0.1), 
                                                    randomization_seed = SEED, 
                                                    detrend = array(data = FALSE, dim = 1), 
                                                    distribution = c("gamma"),
                                                    trend_preservation = array(data = "mixed", dim = 1),
                                                    invalid_value_warnings=TRUE 
                                                    )
                                    )

############################################################
    #NOTE
    ## il parametro lower_threshold domina su wet_threshold
    ## inoltre impostando lo stesso randomization_seed i valori finali effettivamente non cambiano
    # il doppio passaggio
    # pr.ens.hist.BC <- biasCorrection(pr.arcis.hist, pr.ens.hist.BC, precipitation=TRUE, wet.threshold=0.1,....etc.)  
    # pr.ens.hist.BC.mediamodelli <- aggregateGrid(pr.ens.hist.BC, aggr.mem = list(FUN = mean, na.rm = TRUE)) 
    # pr.ens.proj.BC.doppiopassaggio <- biasCorrection(pr.ens.hist.BC.mediamodelli, pr.ens.hist,pr.ens.proj, precipitation=TRUE,....etc.)
    # è teoricamente discutibile e fornisce valori abbastanza diversi da quelli del singolo passaggio
    # per questo viene abbandonato
###########################################################

#armonizzo formato date
#str(dataset_proj.BASD$Dates$start)
dataset_proj.BASD$Dates$start <- as.Date(dataset_proj.BASD$Dates$start)
#str(dataset_proj.BASD$Dates$start)
dataset_proj.BASD$Dates$start <- ymd(dataset_proj.BASD$Dates$start)
#str(dataset_proj.BASD$Dates$start)
dataset_proj.BASD$Dates$start <- as.character(dataset_proj.BASD$Dates$start)
#str(dataset_proj.BASD$Dates$start)

#str(dataset_proj.BASD$Dates$end)
dataset_proj.BASD$Dates$end <- as.Date(dataset_proj.BASD$Dates$end)
#str(dataset_proj.BASD$Dates$end)
dataset_proj.BASD$Dates$end <- ymd(dataset_proj.BASD$Dates$end)
#str(dataset_proj.BASD$Dates$end)
dataset_proj.BASD$Dates$end <- as.character(dataset_proj.BASD$Dates$end)
#str(dataset_proj.BASD$Dates$end)

# TITOLI FILE OUTPUT CON MODELLI BASD
TITOLO <- paste(Sys.getenv("DATA_LOCATION"), Sys.getenv("JOB_DATA_LOCATION"), Sys.getenv("OUTPUTS"), sep = "/")

# Salvataggio in formato NetCDF
grid2nc(dataset_proj.BASD,
        NetCDFOutFile = TITOLO,
        missval = NaN,
        globalAttributes = NULL,
        varAttributes = NULL,
        prec = "float",
        compression = 4,
        shuffle = FALSE,
        verbose = FALSE,
        gridNorthPole = c("39.25", "-162.0"),
        coordBounds = NULL)         

# Resoconto file in output in /var/michea-storage/("JOB_DATA_LOCATION")/output.nc4
