import os
import subprocess
import uuid

# import shutil
from typing import List, Optional

from pipeline.pipeline_handler_factory import Handler, HandlerResult


class MergeFiles(Handler):
    def handle(
        self,
        volume_path: str,
        job_inputs: List[str],
        job_outputs: List[str],
        job_metadata: dict,
        job_data_location: str,
        ci_pipeline_id: int,
        previous_job_data_location: Optional[str],
    ) -> HandlerResult:
        if previous_job_data_location is None:
            raise ValueError(
                "previous_job_data_location not found. The env variable EXECUTED_JOB_HASH may have not "
                "been set by the previous job."
            )

        outputs_path = volume_path + "/" + job_data_location
        inputs_path = volume_path + "/" + previous_job_data_location

        files_to_merge = [inputs_path + "/" + val for val in job_inputs]

        tmp_output_file = outputs_path + "/" + str(uuid.uuid4()) + ".nc"
        output_file = outputs_path + "/" + job_outputs[0]

        success = self.__merge(files_to_merge=files_to_merge, output_file=output_file, tmp_output_file = tmp_output_file)

        return HandlerResult(success=success)

    def __merge(self, files_to_merge: List[str], output_file: str, tmp_output_file: str) -> bool:

        # to merge files with same variable over different time ranges
        # shell out to system command 'cdo mergetime infile1 infile2 infile3 infile4 outfile'
        # https://code.mpimet.mpg.de/projects/cdo/embedded/index.html#x1-1270002.2.9
        exit_code = os.system(f"cdo mergetime {' '.join(files_to_merge)} {tmp_output_file}")
        if exit_code != 0:
            return exit_code == 0

        byte_clim_var = subprocess.check_output(f"cdo -showname {tmp_output_file}", shell=True)
        clim_var = byte_clim_var.decode("utf-8").strip()

        if clim_var == "pr":
          exit_code = os.system(f"cdo expr,'{clim_var}={clim_var}*86400' {tmp_output_file} {output_file}")
          if exit_code != 0:
              return exit_code == 0
        else:
          exit_code = os.system(f"cdo expr,'{clim_var}={clim_var}*1' {tmp_output_file} {output_file}")
          if exit_code != 0:
              return exit_code == 0

        exit_code = os.system(f"rm {tmp_output_file}")
        if exit_code != 0:
            return exit_code == 0

        cdo_sinfon = subprocess.check_output(f"cdo sinfon {output_file}", shell=True)
        cdo_sinfon_indented = "\n " + cdo_sinfon.decode("utf-8").replace("\n", "\n ")
        print(cdo_sinfon_indented)

        return exit_code == 0
