import os
from typing import List, Optional

from pipeline.pipeline_handler_factory import Handler, HandlerResult


class CropFiles(Handler):
    def fill_metadata(self, metadata: dict) -> dict:
        for var in [
            "LON_SIZE",
            "LON_FIRST",
            "LON_INC",
            "LAT_SIZE",
            "LAT_FIRST",
            "LAT_INC",
        ]:
            metadata[var] = os.getenv(var)
        return metadata

    def handle(
        self,
        volume_path: str,
        job_inputs: List[str],
        job_outputs: List[str],
        job_metadata: dict,
        job_data_location: str,
        ci_pipeline_id: int,
        previous_job_data_location: Optional[str],
    ) -> HandlerResult:
        if previous_job_data_location is None:
            raise ValueError(
                "previous_job_data_location not found. The env variable EXECUTED_JOB_HASH may have not "
                "been set by the previous job."
            )
        if len(job_inputs) != len(job_outputs):
            raise ValueError(
                "the INPUTS and OUTPUTS lists must have the same number of elements"
            )

        outputs_path = volume_path + "/" + job_data_location
        inputs_path = volume_path + "/" + previous_job_data_location

        files_to_crop = [inputs_path + "/" + val for val in job_inputs]
        cropped_files = [outputs_path + "/" + val for val in job_outputs]

        lon_size = int(job_metadata["LON_SIZE"])
        lon_first = float(job_metadata["LON_FIRST"])
        lon_inc = float(job_metadata["LON_INC"])
        lat_size = int(job_metadata["LAT_SIZE"])
        lat_first = float(job_metadata["LAT_FIRST"])
        lat_inc = float(job_metadata["LAT_INC"])

        grid = f"""gridtype = lonlat
gridsize = {lon_size * lat_size}
xsize = {lon_size}
ysize = {lat_size}
xname = lon
xlongname = "longitude"
xunits = "degrees_east"
yname = lat
ylongname = "latitude"
yunits = "degrees_north"
xfirst = {lon_first}
xinc = {lon_inc}
yfirst  = {lat_first}
yinc = {lat_inc}"""
        grid_file = outputs_path + "/grid.txt"
        with open(grid_file, "wt") as f:
            f.write(grid)

        for file_to_crop, cropped_file in zip(files_to_crop, cropped_files):
            success = self.__crop(
                grid_file=grid_file,
                file_to_crop=file_to_crop,
                cropped_file=cropped_file,
            )
            if not success:
                return HandlerResult(success=False)

        return HandlerResult(success=True)

    def __crop(self, grid_file: str, file_to_crop: str, cropped_file: str) -> bool:
        # perform a bilinear remapping of fields between grids in spherical coordinates
        # https://code.mpimet.mpg.de/projects/cdo/embedded/index.html#x1-6660002.12.1
        exit_code = os.system(f"cdo remapbil,{grid_file} {file_to_crop} {cropped_file}")
        return exit_code == 0
