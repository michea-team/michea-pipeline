import shutil
from typing import List, Optional

from pipeline.pipeline_handler_factory import Handler, HandlerResult


class CsvValidator(Handler):
    def handle(
        self,
        volume_path: str,
        job_inputs: List[str],
        job_outputs: List[str],
        job_metadata: dict,
        job_data_location: str,
        ci_pipeline_id: int,
        previous_job_data_location: Optional[str],
    ) -> HandlerResult:
        if previous_job_data_location is None:
            raise ValueError(
                "previous_job_data_location not found. The env variable EXECUTED_JOB_HASH may have not "
                "been set by the previous job."
            )

        success = self.__validate(
            file_to_validate=volume_path
            + "/"
            + previous_job_data_location
            + "/"
            + job_inputs[0],
            validated_file_path=volume_path + "/" + job_data_location,
        )

        return HandlerResult(success=success)

    def __validate(self, file_to_validate: str, validated_file_path: str) -> bool:
        shutil.copy(file_to_validate, validated_file_path)
        return True
