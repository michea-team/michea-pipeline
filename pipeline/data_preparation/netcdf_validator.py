# import shutil
from typing import List, Optional

from pipeline.pipeline_handler_factory import Handler, HandlerResult


class NetCDFValidator(Handler):
    def handle(
        self,
        volume_path: str,
        job_inputs: List[str],
        job_outputs: List[str],
        job_metadata: dict,
        job_data_location: str,
        ci_pipeline_id: int,
        previous_job_data_location: Optional[str],
    ) -> HandlerResult:
        success = self.__validate()

        return HandlerResult(success=success)

    def __validate(self) -> bool:
        return True
