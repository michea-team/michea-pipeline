from abc import abstractmethod
from pydoc import locate
from typing import Iterable, List, Optional, Protocol, TypedDict


class HandlerResult(TypedDict):
    success: bool
    data: Optional[Iterable]


class Handler(Protocol):
    def __init__(self):
        pass

    @abstractmethod
    def handle(
        self,
        volume_path: str,
        job_inputs: List[str],
        job_outputs: List[str],
        job_metadata: dict,
        job_data_location: str,
        ci_pipeline_id: int,
        previous_job_data_location: Optional[str],
    ) -> HandlerResult:
        raise NotImplementedError

    def fill_metadata(self, metadata: dict) -> dict:
        return metadata


def pipeline_handler_factory(class_name: Optional[str]) -> Handler:
    handler_class = locate("pipeline." + class_name)
    return handler_class()
